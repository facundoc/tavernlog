=========
TavernLog
=========

Keep a log of you Hearthstone games in an html site.

This script parses a Hearthstone log file and creates an html site with all the
games you played.

Install
-------

Get the code, create a new environment and install dependencies::

   $ git clone git@bitbucket.org:facundoc/tavernlog.git
   $ cd tavernlog
   $ virtualenv .
   $ . bin/activate
   $ pip install -r requirements.txt


Run
---

Change the paths in the 'config.ini' file and run the script::

   $ python tavernlog/tavernlog.py
