import argparse
import configparser
import datetime
import logging
import os

from jinja2 import Environment, PackageLoader, select_autoescape
from hsreplay.document import HSReplayDocument

"""
/mnt/music/games/wine_hearthstone/drive_c/Program Files (x86)/Hearthstone/Logs/LoadingScreen.log
/mnt/music/games/wine_hearthstone/drive_c/Program Files (x86)/Hearthstone/Logs/Bob.log
/mnt/music/games/wine_hearthstone/drive_c/Program Files (x86)/Hearthstone/Logs/Asset.log
/mnt/music/games/wine_hearthstone/drive_c/Program Files (x86)/Hearthstone/Logs/Power.log
/mnt/music/games/wine_hearthstone/drive_c/Program Files (x86)/Hearthstone/Logs/Gameplay.log
/mnt/music/games/wine_hearthstone/drive_c/Program Files (x86)/Hearthstone/Logs/Zone.log
/mnt/music/games/wine_hearthstone/drive_c/Program Files (x86)/Hearthstone/Logs/BattleNet.log
"""


class TavernLog:

    def __init__(self, args):
        self.args = args
        self.config = configparser.ConfigParser()
        self.config.read('tavernlog/config.ini')

        logging.basicConfig(filename=self.config['tavernlog']['log_filename'],
                            level=getattr(logging, self.config['tavernlog']['log_level']))

    def get_games(self, filename=None):
        "Returns the games parsed from the log file"

        if filename is None:
            filename = os.path.join(self.config['tavernlog']['game_path'],
                                    self.config['tavernlog']['power_log'])

        logging.info('Processing log file %s', filename)

        with open(filename) as games_log:
            hs_replay_doc = HSReplayDocument.from_log_file(games_log, processor="GameState",
                                                           date=None, build=None)

        xml_path = self.config['tavernlog']['xml_path']
        if xml_path:
            today = datetime.date.today()
            xml_file = os.path.join(xml_path, 'games_{}.xml'.format(today.isoformat()))
            with open(xml_file, 'w') as o:
                o.write(hs_replay_doc.to_xml())

        return hs_replay_doc.games

    def update_html(self):
        jinja_env = Environment(loader=PackageLoader('tavernlog', 'templates'),
                                autoescape=select_autoescape(['html', 'xml']))
        games_template = jinja_env.get_template('games.html.j2')

        out_filename = os.path.join(self.config['tavernlog']['html_path'], 'games.html')
        with open(out_filename, 'w') as o:
            o.write(games_template.render(games=self.games))

    def run(self):
        self.games = self.get_games()
        self.update_html()


def main(args):
    hearth_log = TavernLog(args)
    hearth_log.run()
    return


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Open hearthstone tracker')

    args = args_parser.parse_args()
    main(args)
